#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
"""

# imports
from __init__ import *


if __name__ == "__main__":
    # do some:
    sca = Scalar()
    sca.csvfile()
    logging.info("tempo[s],velocidade[m/s],posicao[m]")
    for i in range(0, 61):
        i = (i/10)
        sca = Scalar(t=i)
        logging.info("{:.2f},{:.4f},{:.4f}".format(i, sca.velocidade(), sca.posicao()))
